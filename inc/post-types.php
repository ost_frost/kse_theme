<?php

add_action('init', 'register_post_types');
function register_post_types(){
    register_post_type('kse_research', array(
        'label'  => __('Research', 'kse'),
        'labels' => array(
            'name'               => __('Researches', 'kse'), // основное название для типа записи
            'singular_name'      => __('Research', 'kse'), // название для одной записи этого типа
            'add_new'            => __('Add new', 'kse'), // для добавления новой записи
            'add_new_item'       => __('Add new research', 'kse'), // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => __('Edit research', 'kse'), // для редактирования типа записи
            'new_item'           => __('New research', 'kse'), // текст новой записи
            'view_item'          => __('View research', 'kse'), // для просмотра записи этого типа.
            'search_items'       => __('Search research', 'kse'), // для поиска по этим типам записи
            'not_found'          => __('Not found', 'kse'), // если в результате поиска ничего не было найдено
            'not_found_in_trash' => __('Not found in trash', 'kse'), // если не было найдено в корзине
            'parent_item_colon'  => __('Parent Item:', 'kse'), // для родителей (у древовидных типов)
            'menu_name'          => __('Research', 'kse'), // название меню
        ),
        'public'              => true,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'supports'            => array('title','editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array(),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ) );
    
    register_post_type('kse_person', array(
        'label'  => __('Person', 'kse'),
        'labels' => array(
            'name'               => __('Persons', 'kse'), // основное название для типа записи
            'singular_name'      => __('Person', 'kse'), // название для одной записи этого типа
            'add_new'            => __('Add new', 'kse'), // для добавления новой записи
            'add_new_item'       => __('Add new person', 'kse'), // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => __('Edit person', 'kse'), // для редактирования типа записи
            'new_item'           => __('New person', 'kse'), // текст новой записи
            'view_item'          => __('View person', 'kse'), // для просмотра записи этого типа.
            'search_items'       => __('Search person', 'kse'), // для поиска по этим типам записи
            'not_found'          => __('Not found', 'kse'), // если в результате поиска ничего не было найдено
            'not_found_in_trash' => __('Not found in trash', 'kse'), // если не было найдено в корзине
            'parent_item_colon'  => __('Parent Item:', 'kse'), // для родителей (у древовидных типов)
            'menu_name'          => __('Person', 'kse'), // название меню
        ),
        'public'              => true,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'supports'            => array('title','editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array(),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ) );
}