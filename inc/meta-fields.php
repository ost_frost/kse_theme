<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach' );
function crb_attach() {
    /*
     * General meta fields
     */
    Container::make('post_meta', __('General data', 'kse'))
        ->add_fields([
            Field::make('color', 'kse_post_color'),
        ]);


    /*
     * Research page meta fields
     */
//    Container::make('post_meta', __('Research data', 'kse'))
//        ->where( 'post_type', '=', 'page' )
//        ->where( 'post_template', '=', 'page-templates/research.php' )
//        ->add_fields([
//        ]);


    /*
     * Research meta fields
     */
    Container::make('post_meta', __('Research data', 'kse'))
        ->where( 'post_type', '=', 'kse_research' )
        ->add_fields([
            Field::make('association', 'kse_research_author', __('Author', 'kse'))
                ->set_types([['type' => 'post', 'post_type' => 'kse_person']])
                ->set_max(1),
            Field::make('radio', 'kse_research_type', 'Type' )
                ->add_options( array(
                    'academic' => 'Academic',
                    'policy' => 'Policy',
                )),
            Field::make('radio', 'kse_research_tab', 'Tab' )
                ->add_options( array(
                    'latest_article' => 'Latest article',
                    'project' => 'Project',
                ))
        ]);
}