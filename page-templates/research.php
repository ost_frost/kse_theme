<?php
/* Template Name: Research */

get_header();

$posts = get_posts([
    'post_type'   => 'kse_research',
    'suppress_filters' => true,
    ]);

$academic_research_articles_html = '';
$academic_research_projects_html = '';
$policy_research_articles_html = '';
$policy_research_projects_html = '';

foreach($posts as $post){
    $type = carbon_get($post->ID, 'kse_research_type', 'post_meta');
    $tab = carbon_get($post->ID, 'kse_research_tab', 'post_meta');

    if ($type == 'academic'){
        if ($tab == 'latest_article'){
            $academic_research_articles_html .= $post->post_content;
        } elseif ($tab == 'project'){
            $academic_research_projects_html .= $post->post_content;
        }
    } elseif ($type == 'policy'){
        if ($tab == 'latest_article'){
            $policy_research_articles_html .= $post->post_content;
        } elseif ($tab == 'project'){
            $policy_research_projects_html .= $post->post_content;
        }
    }
}

wp_reset_postdata();

get_sidebar();
get_footer();
